////
////  CalculatorManager.swift
////  CalculatorApp
////
////  Created by Oleh Makhobei on 06.09.2021.
////
//
//import Foundation
//class CalculationManager: CalculatorLogicProtocol {
//    
//    
//    var viewController : CalculatorUIProtocol?
//    
//    var savedValue : String = ""
//    var operand1 : Double? = nil
//    var operation : CalcOperation? = nil
//    var result : Double? = nil
//    
//    init(ui: CalculatorUIProtocol){
//        viewController = ui
//    }
//    func didTapOn(key: CalcButton) {
//        let newOperation = key.operation
//        switch newOperation {
//        case .CLEAN:
//              operand1 = nil
//              operation = nil
//              savedValue = ""
//              result = nil
//              viewController?.displayValue("")
//        case .PLUS,.MINUS,.DIVIDE,.MULTIPLY,.PERCENT:
//              operand1 = Double(savedValue)
//              operation = newOperation
//               savedValue = ""
//              viewController?.displayValue("")
//
//            if newOperation == .PERCENT {
//                  result = operand1! / 100
//                  savedValue = String(result!)
//                  viewController?.displayValue(String(result!))
//            }else{
//                if result != nil {
//                    operand1 = result
//                    viewController?.displayValue(String(result!))
//                }
//            }
//
//        case .EQUAL:
//             if operand1 != nil && operation != nil {
//                  switch operation{
//                  case  .PLUS:
//                          result = operand1! + Double(savedValue)!
//                  case  .MINUS:
//                          result = operand1! - Double(savedValue)!
//                  case  .DIVIDE:
//                          result = operand1! / Double(savedValue)!
//                  case  .MULTIPLY:
//                          result = operand1! * Double(savedValue)!
//                  case  .PERCENT:
//                          result = operand1! / 100
//                          savedValue = String(result!)
//                      default :
//                         print("many more operations")
//                        }
//              //  print(operation as Any)
//              viewController?.displayValue(String(result!))
//              savedValue = String (result!)
//           }
//             else{
//                  viewController?.displayValue(String(savedValue))
//
//                  }
//
//        case .SIGN:
//              if(savedValue.starts(with: "-")){
//                  savedValue = String(savedValue.dropFirst())
//              }else{
//                  savedValue = "-" + savedValue
//              }
//              viewController?.displayValue(savedValue)
//          //    input?.calcLbl.text = String(savedValue)
//          
//              
//          default:
//              
//            savedValue += key.title
//              viewController?.displayValue(savedValue)
//              
//              //Nested switch is for case, when user tapped some operetion few times without pressing "=". for ex: 2+2+2+2...
//            if operation == CalcOperation.PLUS {
//                viewController?.displayValue("+ \(savedValue ) ")
//                  result = (operand1!) + Double(savedValue)!
//                  
//               }
//            if operation == CalcOperation.MINUS {
//                  
//                  result = (operand1!) - Double(savedValue )!
//               }
//            if operation == CalcOperation.MULTIPLY {
//              
//                  result = (operand1!) * Double(savedValue)!
//               }
//            if operation == CalcOperation.DIVIDE {
//
//                  result = (operand1!) / Double(savedValue)!
//               }
//            if operation == CalcOperation.PERCENT {
//                 result = operand1! / 100
//        }
//      }
//    }
//    
//    static func generateDataSource() -> [CalcButton]{
//        let buttons = [
//            CalcButton(title:"0", backGround: .gray, operation: .DIGIT, fontColor: .white,fontStyle: .systemFont(ofSize: 120,weight: .light) ),
//            CalcButton(title:"AC", backGround: .lightGray, operation: .CLEAN, fontColor: .black, fontStyle:.systemFont(ofSize: 35,weight: .light)),
//            CalcButton(title:"+/-",backGround: .lightGray,operation: .SIGN , fontColor: .black, fontStyle:.systemFont(ofSize: 35,weight: .light)),
//            CalcButton(title:"%", backGround: .lightGray, operation: .PERCENT, fontColor: .black, fontStyle:.systemFont(ofSize: 35,weight: .light)),
//            CalcButton(title:"/", backGround: .orange, operation: .DIVIDE, fontColor: .white, fontStyle:.systemFont(ofSize: 35,weight: .light)),
//            CalcButton(title:"7", backGround: .lightGray, operation: .DIGIT, fontColor: .black, fontStyle:.systemFont(ofSize: 35,weight: .light)),
//            CalcButton(title:"8", backGround: .lightGray, operation: .DIGIT, fontColor: .black, fontStyle:.systemFont(ofSize: 35,weight: .light)),
//            CalcButton(title:"9", backGround: .lightGray, operation: .DIGIT, fontColor: .black, fontStyle:.systemFont(ofSize: 35,weight: .light)),
//            CalcButton(title:"x", backGround: .orange, operation: .MULTIPLY, fontColor: .white, fontStyle:.systemFont(ofSize: 35,weight: .light)),
//            CalcButton(title:"4", backGround: .lightGray, operation: .DIGIT, fontColor: .black, fontStyle:.systemFont(ofSize: 35,weight: .light)),
//            CalcButton(title:"5", backGround: .lightGray, operation: .DIGIT, fontColor: .black, fontStyle:.systemFont(ofSize: 35,weight: .light)),
//            CalcButton(title:"6", backGround: .lightGray, operation: .DIGIT, fontColor: .black, fontStyle:.systemFont(ofSize: 35,weight: .light)),
//            CalcButton(title:"-", backGround: .orange, operation: .MINUS, fontColor: .white, fontStyle:.systemFont(ofSize: 35,weight: .light)),
//            CalcButton(title:"1", backGround: .lightGray, operation: .DIGIT, fontColor: .black, fontStyle:.systemFont(ofSize: 35,weight: .light)),
//            CalcButton(title:"2", backGround: .lightGray, operation: .DIGIT, fontColor: .black, fontStyle:.systemFont(ofSize: 35,weight: .light)),
//            CalcButton(title:"3", backGround: .lightGray, operation: .DIGIT, fontColor: .black, fontStyle:.systemFont(ofSize: 35,weight: .light)),
//            CalcButton(title:"+", backGround: .orange, operation: .PLUS, fontColor: .white, fontStyle:.systemFont(ofSize: 35,weight: .light)),
//            CalcButton(title:"0", backGround: .lightGray, operation: .DIGIT, fontColor: .black, fontStyle:.systemFont(ofSize: 35,weight: .light)),
//            CalcButton(title:".", backGround: .lightGray, operation: .DIGIT, fontColor: .black, fontStyle:.systemFont(ofSize: 35,weight: .light)),
//            CalcButton(title:"=", backGround: .orange, operation: .EQUAL, fontColor: .white, fontStyle:.systemFont(ofSize: 35,weight: .light))]
//            
//        
//        return buttons
//    }
//}
//
//
//
