
//  Created by Olena Makhobei on 11.10.2021.
import Foundation
import UIKit

class Validation :CalculatorInputValidation {
    
    func isValidOperation(key: String, array: inout [CalcValue]) -> Bool {
        let last = array.last
        if let lastType = last?.operation{
            if lastType != .DIGIT && lastType != .RIGHTPRANT && lastType != .LEFTPRANT{
                upDateInput(key: key, array: &array)
                return false
            }
            else if lastType == .LEFTPRANT{
                return false
            }
        }
        return true
    }
    
    func upDateInput(key: String, array: inout [CalcValue]) {
        if array.count > 1{
        array.removeLast()
        array.append(CalcValue(operation: CalcOperation(rawValue: key)!, value: nil))
          
        }
    }
    
    func getResultString(array : [CalcValue]) -> String {
        let expression = generateString(false, array: array)
        return expression

    }
    
    func generateString(_ isDouble: Bool, array : [CalcValue]) -> String {
        var strings:[String] = []

        for value in array{
            if(value.operation == CalcOperation.DIGIT){
                if   isDouble  {
                    if let notnull = value.value {
                        if(!notnull.isEmpty){
                            strings.append(String(Double(notnull)!))
                        }
                    }
                }else{
                    strings.append(value.value ?? "")
                }
            }else {
                strings.append(value.operation.rawValue)
            }
        }
        return strings.joined(separator: "")
    }
    
    func isValidDigitwithPoint(key : String, array : [CalcValue])->Bool{
        let last = array.last
        if  let lastType = last?.value{
            if lastType.contains(".") && key == "."{return false}
        }
        return true
        }
    
    func isValidDigitAfterOperation(key : String, array : [CalcValue]) -> Bool{
        let last = array.last
        if let lastValue = last?.operation{
            if lastValue != .DIGIT &&  lastValue != .RIGHTPRANT && lastValue != .LEFTPRANT && key == "."
            {return false}
        }
        return true
    }
 
    
    func isValidAmountOfBrackets(array:[CalcValue], key: CalcButton)->Bool {
        let leftParentesisCount = array.filter({$0.operation == CalcOperation.LEFTPRANT}).count
        let rightParentesisCount = array.filter({$0.operation == CalcOperation.RIGHTPRANT}).count
        if key.operation == .LEFTPRANT
                   {
                       return true
                                   }
                   else if  key.operation == .RIGHTPRANT{
           
                   if leftParentesisCount > rightParentesisCount{
                       return true
                   }
                   else{
                       return false
                   }
                }
                   return true
    }
    
    func isValidValueWithBrackets(array:[CalcValue], string:String) -> Bool{
        return true
    }
    
    func checkingIfAfterOperationsIsDigit(array: [CalcValue]) -> Bool{
        let last = array.last
        if let lastValue = last?.operation{
            if   lastValue != .RIGHTPRANT && lastValue != .DIGIT{
                return false
            }
            else
                {return true}
        }
        return true
    }
    
    func bracketsAmount(array: [CalcValue])->Bool{
        let leftParentesisCount = array.filter({$0.operation == CalcOperation.LEFTPRANT}).count
        let rightParentesisCount = array.filter({$0.operation == CalcOperation.RIGHTPRANT}).count
        
        if leftParentesisCount != rightParentesisCount{
            return false
        }
        return true
    }
    
    func checkIfAfterPointIsDigit (array: [CalcValue], key : CalcButton)-> Bool {
        let last = array.last
        if let lasttype = last?.operation{
            if lasttype == .DIGIT{
                let rawValue =  last?.value
                let checkpoint = rawValue?.last
                if checkpoint == "." && key.operation  != .DIGIT
                {return false}
            }
            }
        
     return true
    }
    
    func checkIfLefPranIsAfterOper(array:[CalcValue], key: CalcButton)->Bool{
        let lastInArray = array.last
        if  let lastType = lastInArray?.operation{
            if lastType == .DIGIT && key.operation == .LEFTPRANT {
                return false
            }
        }
        return true
    }
    
    func limitLengthOfInputDigit(array: [CalcValue], key: CalcButton)->Bool{
        let lastInArray = array.last
        if let lastType = lastInArray?.operation{
            if lastType == .DIGIT {
                guard let  rawLastType  = lastInArray?.value else {
                    return false
                }
                if rawLastType.contains("."){
                    
                    if let firstIndex = rawLastType.firstIndex(of: ".") {
                        let indexOfPoint  = rawLastType.distance(from: rawLastType.startIndex, to: firstIndex)
                        print("index: ", indexOfPoint)
                        let rizn = (indexOfPoint) + 5
                          if indexOfPoint < 6 && rawLastType.count > rizn
                           {
                            print(rawLastType)
                            return false
                            }
                        }
                     }
                else {
                    if rawLastType.count > 4 && key.title != "."
                {return false}
                }
                   }
               }
        return true
}
     
    
    
    // General cgecking function
func ckechInputPrant(array: [CalcValue], key : CalcButton)->Bool{
 guard  checkIfLefPranIsAfterOper(array: array, key: key)
   &&   isValidAmountOfBrackets(array: array, key: key)
   else{return false}
 return true
 }

func checkInputOperation(array: inout [CalcValue], key: CalcButton)->Bool{
 guard   checkIfAfterPointIsDigit(array: array, key: key)
   &&    isValidOperation(key: key.operation.rawValue, array: &array)
   else {return false}
 return true
 }
 
func checkBracketsAmountWhenEqual (array: [CalcValue], key: CalcButton)->Bool{
 guard   bracketsAmount(array: array)
   &&    checkingIfAfterOperationsIsDigit(array: array)
   else {return false}
 return true
 }

func checkInputDigits(array: [CalcValue], key: CalcButton)->Bool{
 guard  checkIfAfterPointIsDigit(array: array, key: key)
   &&   isValidDigitwithPoint(key: key.title, array: array)
   &&   isValidDigitAfterOperation(key: key.title, array: array)
   &&   limitLengthOfInputDigit( array: array, key: key)
   &&   limitLengthOfInputDigit(array: array, key: key)
   else {return false}
 return true
    }
    

    
}

extension   String {
    func intIndex (_ str:Character)-> Int{
        return self.firstIndex( of: str)?.utf16Offset(in: self) ??  (-1)
    }
}
    
