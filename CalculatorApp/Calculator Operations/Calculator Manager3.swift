
import Foundation
import UIKit

class CalculationManager3: CalculatorLogicProtocol {
    var valueArray : [CalcValue] = []// this array needed for checking correct input
    private weak var viewController : CalculatorUIProtocol?
   
    var validation = Validation()
    
    init(ui: CalculatorUIProtocol){
        viewController = ui
    }
    
    func didTapOn(key: CalcButton) {
        switch key.operation {
        
                case .CLEAN:
                    viewController?.displayValue("")
                    valueArray.removeAll()
                    
                case .PLUS,.MINUS,.DIVIDE,.MULTIPLY:
                    if validation.checkInputOperation(array: &valueArray, key: key) == true{
                        appendElemetInValueArray(expression: key, digit: nil)
                    }
                    printResult()
                   
                case .RIGHTPRANT, .LEFTPRANT:
                    if  validation.ckechInputPrant(array: valueArray, key: key) == true{
                        appendElemetInValueArray(expression: key, digit: nil)
                    }
                    
                case .PERCENT :
                    if valueArray.count < 2 {
                        let last = valueArray.last
                        if let lastValue = last?.value {
                                let lastRawValue = lastValue
                                let num  = Double(lastRawValue)! * 0.01
                                valueArray.removeLast()
                                valueArray.append(CalcValue(operation: CalcOperation.DIGIT, value: String(num)))
                                printResult()
                            }
                        }
                    
                case .EQUAL :
                    if validation.checkBracketsAmountWhenEqual(array: valueArray, key: key) == true{
                    doMathFormGeneratedString()
                    }
                    
                case .SIGN:
                    let last = valueArray.last
                    if  let lastType = last?.value{
                        if lastType.starts(with: "-"){
                            let modifiedValue = String(lastType.dropFirst())
                            valueArray.removeLast()
                            valueArray.append(CalcValue(operation: CalcOperation.DIGIT, value: modifiedValue))
                            }
                    
                        else {
                            let modifiedValue = "-" + lastType
                            valueArray.removeLast()
                            valueArray.append(CalcValue(operation: CalcOperation.DIGIT, value: modifiedValue))
                        }
                  printResult()
                    }

                default://--Mark Digit CASE

                    if  validation.checkInputDigits(array: valueArray, key: key)  == true{
                    let lastInArray = valueArray.last
                        
                    if valueArray.isEmpty{
                        if key.title != "." {
                        valueArray.append(CalcValue(operation: CalcOperation.DIGIT, value: key.title))
                        }
                    }
                    else if  let lastOperation = lastInArray?.operation{
                        if lastOperation != .DIGIT {
                            valueArray.append(CalcValue(operation: CalcOperation.DIGIT, value: key.title))
                    }
                        if lastOperation == .DIGIT{
                            updateLastChar(key.title)
                        }
                    }
                    printResult()
        }
    }
}
    
    
    
    func doMathFormGeneratedString(){
        let generatedExpression = validation.generateString(true, array: valueArray)
        let fixedExpr = generatedExpression.replacingOccurrences(of: "x", with: "*")
        let mathExpression = NSExpression(format: fixedExpr)
        let mathValue = mathExpression.expressionValue(with: nil, context: nil) as? Double
        // Do the math here
        
        let formatter = NumberFormatter()
        formatter.maximumFractionDigits = 2
        let roundedValue = formatter.string(from: NSNumber(value: mathValue!))
        viewController?.displayValue(String(roundedValue!))
        valueArray.removeAll()
        appendElemetInValueArray(expression: nil, digit: roundedValue)
        
    }
    
  
    func appendElemetInValueArray(expression: CalcButton?, digit : String?){
        if let newDigit = digit {
            valueArray.append(CalcValue(operation: CalcOperation.DIGIT, value: newDigit))
        }
        if let newOperation = expression{
            valueArray.append(CalcValue(operation: newOperation.operation, value: nil))
        
    }
        printResult()
    }
    
   
    func updateLastChar(_ key :String){
        let last = valueArray.last
        if let lasttype = last?.operation{
            if lasttype == .DIGIT{
                let lastRawValue = last?.value
                valueArray.removeLast()
                let updatedValue = lastRawValue! + key
                valueArray.append(CalcValue(operation: CalcOperation.DIGIT, value: updatedValue))
               
            }
        }
    }
     
    private func printResult(){
        let printedRes = validation.getResultString(array: valueArray)
        viewController?.displayValue(printedRes)
        }

    static func generateDataSource() -> [CalcButton]{
        let buttons = [
            CalcButton(title:"0", backGround: .gray, operation: .DIGIT, fontColor: .white,fontStyle: .systemFont(ofSize: 45,weight: .light) ),
            CalcButton(title:"AC", backGround: .lightGray, operation: .CLEAN, fontColor: .black, fontStyle:.systemFont(ofSize: 35,weight: .light)),
            CalcButton(title:"+/-",backGround: .lightGray,operation: .SIGN , fontColor: .black, fontStyle:.systemFont(ofSize: 35,weight: .light)),
            CalcButton(title:"%", backGround: .lightGray, operation: .PERCENT, fontColor: .black, fontStyle:.systemFont(ofSize: 35,weight: .light)),
            CalcButton(title:"/", backGround: .orange, operation: .DIVIDE, fontColor: .white, fontStyle:.systemFont(ofSize: 35,weight: .light)),
            CalcButton(title:"7", backGround: .lightGray, operation: .DIGIT, fontColor: .black, fontStyle:.systemFont(ofSize: 35,weight: .light)),
            CalcButton(title:"8", backGround: .lightGray, operation: .DIGIT, fontColor: .black, fontStyle:.systemFont(ofSize: 35,weight: .light)),
            CalcButton(title:"9", backGround: .lightGray, operation: .DIGIT, fontColor: .black, fontStyle:.systemFont(ofSize: 35,weight: .light)),
            CalcButton(title:"x", backGround: .orange, operation: .MULTIPLY, fontColor: .white, fontStyle:.systemFont(ofSize: 35,weight: .light)),
            CalcButton(title:"4", backGround: .lightGray, operation: .DIGIT, fontColor: .black, fontStyle:.systemFont(ofSize: 35,weight: .light)),
            CalcButton(title:"5", backGround: .lightGray, operation: .DIGIT, fontColor: .black, fontStyle:.systemFont(ofSize: 35,weight: .light)),
            CalcButton(title:"6", backGround: .lightGray, operation: .DIGIT, fontColor: .black, fontStyle:.systemFont(ofSize: 35,weight: .light)),
            CalcButton(title:"-", backGround: .orange, operation: .MINUS, fontColor: .white, fontStyle:.systemFont(ofSize: 35,weight: .light)),
            CalcButton(title:"1", backGround: .lightGray, operation: .DIGIT, fontColor: .black, fontStyle:.systemFont(ofSize: 35,weight: .light)),
            CalcButton(title:"2", backGround: .lightGray, operation: .DIGIT, fontColor: .black, fontStyle:.systemFont(ofSize: 35,weight: .light)),
            CalcButton(title:"3", backGround: .lightGray, operation: .DIGIT, fontColor: .black, fontStyle:.systemFont(ofSize: 35,weight: .light)),
            CalcButton(title:"+", backGround: .orange, operation: .PLUS, fontColor: .white, fontStyle:.systemFont(ofSize: 35,weight: .light)),
            CalcButton(title:"0", backGround: .lightGray, operation: .DIGIT, fontColor: .black, fontStyle:.systemFont(ofSize: 35,weight: .light)),
            CalcButton(title:".", backGround: .lightGray, operation: .DIGIT, fontColor: .black, fontStyle:.systemFont(ofSize: 35,weight: .light)),
            CalcButton(title:"(", backGround: .lightGray, operation: .LEFTPRANT, fontColor: .black, fontStyle:.systemFont(ofSize: 35,weight: .light)),
            CalcButton(title:")", backGround: .lightGray, operation: .RIGHTPRANT, fontColor: .black, fontStyle:.systemFont(ofSize: 35,weight: .light)),
            CalcButton(title:"=", backGround: .orange, operation: .EQUAL, fontColor: .white, fontStyle:.systemFont(ofSize: 35,weight: .light))]
        
        
        return buttons
    }
    
    
}



