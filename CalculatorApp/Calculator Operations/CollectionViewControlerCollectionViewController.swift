//
//  CollectionViewControlerCollectionViewController.swift
//  CalculatorApp
//
//  Created by Olena Makhobei on 25.08.2021.
//

import UIKit

class CollectionViewControlerCollectionViewController: UICollectionViewController ,UICollectionViewDelegateFlowLayout,CalculatorUIProtocol{
    func displayValue(_ value: String) {
        input?.calcLbl.text = value
        
    }
    
    var textField : String = "0"
    var input:CollectionViewCell? = nil
    let dataSource = CalculationManager3.generateDataSource()
    var logic : CalculatorLogicProtocol?
    var value : Int?
  
     
    override func viewDidLoad() {
        super.viewDidLoad()
        collectionView.delegate = self
        collectionView.collectionViewLayout = createLayout()
        collectionView.backgroundColor = .darkGray
        logic = CalculationManager3(ui: self) 

    }
    func createLayout() -> UICollectionViewCompositionalLayout{
        
        let itemSquare = NSCollectionLayoutItem(layoutSize: NSCollectionLayoutSize(widthDimension: .fractionalWidth(0.25), heightDimension: .fractionalHeight(1)))
        itemSquare.contentInsets = NSDirectionalEdgeInsets(top: 1, leading: 1, bottom: 1, trailing: 1 )
        
        let firstItem = NSCollectionLayoutItem(layoutSize: NSCollectionLayoutSize(widthDimension: .fractionalWidth(1), heightDimension: .fractionalHeight(0.33)))
        
        let narrowItem = NSCollectionLayoutItem(layoutSize: NSCollectionLayoutSize(widthDimension: .fractionalWidth(0.125), heightDimension: .fractionalHeight(1)))
        
        narrowItem.contentInsets = NSDirectionalEdgeInsets(top: 1, leading: 1, bottom: 1, trailing: 1)
        
        let lastItem = NSCollectionLayoutItem(layoutSize: NSCollectionLayoutSize(widthDimension: .fractionalWidth(0.25), heightDimension: .fractionalHeight(1)))
        
        lastItem.contentInsets = NSDirectionalEdgeInsets(top: 1, leading: 1, bottom: 1, trailing: 1)
        
        let horizontalSquareStack = NSCollectionLayoutGroup.horizontal(layoutSize: NSCollectionLayoutSize(widthDimension: .fractionalWidth(1), heightDimension: .fractionalHeight(0.5)), subitem: itemSquare, count: 4)
        
        let verticalSquareStack = NSCollectionLayoutGroup.vertical(layoutSize: NSCollectionLayoutSize(widthDimension: .fractionalWidth(1), heightDimension: .fractionalHeight(0.49)), subitem: horizontalSquareStack, count: 4)
      
        let narrowStack = NSCollectionLayoutGroup.horizontal(layoutSize: NSCollectionLayoutSize(widthDimension: .fractionalWidth(0.25), heightDimension:.fractionalHeight(1)), subitem: narrowItem, count: 2)
        
        let pairStack = NSCollectionLayoutGroup.horizontal(layoutSize: NSCollectionLayoutSize(widthDimension: .fractionalWidth(0.5), heightDimension: .fractionalHeight(1)), subitem:itemSquare, count: 2)
        
        let bottomStack = NSCollectionLayoutGroup.horizontal(layoutSize: NSCollectionLayoutSize(widthDimension: .fractionalWidth(1), heightDimension: .fractionalHeight(0.15)), subitems:[pairStack, narrowStack,itemSquare])
        
        let generalGroup = NSCollectionLayoutGroup.vertical(layoutSize: NSCollectionLayoutSize(widthDimension: .fractionalWidth(1), heightDimension: .fractionalHeight(1)), subitems:[firstItem,verticalSquareStack,bottomStack])
        
        let section = NSCollectionLayoutSection(group: generalGroup)
        
        return  UICollectionViewCompositionalLayout(section: section)
        
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return dataSource.count
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        var cell = UICollectionViewCell()

        if let calculatorCell =  collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath) as? CollectionViewCell
        {

            if indexPath.row == 0 {
                input = calculatorCell
                calculatorCell.calcLbl.textAlignment = .right
            }
            calculatorCell.configure(with: dataSource[indexPath.row])
            cell = calculatorCell
        }
        
        return cell
    }

    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let value = dataSource[indexPath.row]
        //Function give back the result for some mathematics operation. Would be calling in case "+","-", etc
        
        logic?.didTapOn(key: value)
        
        
     
    }
}


