////
////  CalculatorManager2.swift
////  CalculatorApp
////
////  Created by Oleh Makhobei on 13.09.2021.
////
//
//import Foundation
//import UIKit
//
//class CalculationManager2: CalculatorLogicProtocol {
//    private var valueArray : [CalcValue] = []
//    private weak var viewController : CalculatorUIProtocol?
//    private let expressionCalc = ExpressionCalculator()
//    private var savedValue : String = ""
//    var generalCalcStrings : String = ""
//    
//    init(ui: CalculatorUIProtocol){
//        viewController = ui
//    }
//    
//    func  upDateInput(key: String){
//        if valueArray.count > 1{
//        valueArray.removeLast()
//        valueArray.append(CalcValue(operation: CalcOperation(rawValue: key)!, value: nil))
//        printResult()        }
//
//    }// Update the array in isValidExpression
//    func isValidOperation(key: String)-> Bool{
//        if savedValue == ""{
//        let last = valueArray.last?.operation.rawValue
//        if key == last {
//                return false
//            }
//        else if key != last && (key == ")" || key == " ("){
//            upDateInput(key: key)
//            return false
//        }
//       
//    }
//        return true
//    }// Guarding correct typing of expression(without bracket and point)
//    func isValidExpression() -> Bool{
//        
//        if ( valueArray.count <= 2 && savedValue == ""){
//            return false
//        }else{
//            return true
//        }
//    } //Check if the last operrations is correct -> Ex:" 5 + = " is not correct
//    func isValidDigitwithPoint(key : String)->Bool{
//        if savedValue.contains(".") && key == "."
//        {
//            return false
//            
//        }
//        return true
//    } //Checking coorect typing of Double digigit WITHOUT TWO POINTS
//    func appendElemetInValueArray(expression: CalcButton){
//        valueArray.append(CalcValue(operation: CalcOperation.DIGIT, value: savedValue))
//        valueArray.append(CalcValue(operation: expression.operation, value: nil))
//
//        savedValue = ""
//        printResult()
//    }
//    func amountOfBrackets(key: CalcButton)->Bool{
//        let leftParentesisCount = valueArray.filter({$0.operation == CalcOperation.LEFTPRANT}).count
//        let rightParentesisCount = valueArray.filter({$0.operation == CalcOperation.RIGHTPRANT}).count
//        if key.operation == .LEFTPRANT
//        {
//            return true
//                        }
//        else if  key.operation == .RIGHTPRANT{
//        
//        if leftParentesisCount > rightParentesisCount{
//            return true
//        }
//        else{
//            return false
//        }
//     }
//        return true
//    }
//    
//    func didTapOn(key: CalcButton) {
//        switch key.operation {
//        case .CLEAN:
//            savedValue = ""
//            viewController?.displayValue("")
//            valueArray.removeAll()
//            generalCalcStrings.removeAll()
//
//        case .PLUS,.MINUS,.DIVIDE,.MULTIPLY:
//            guard  isValidOperation(key: key.operation.rawValue) else
//            {return}
//            appendElemetInValueArray(expression: key)
//            generalCalcStrings.append(key.operation.rawValue)
//
//        case .RIGHTPRANT, .LEFTPRANT:
//            guard amountOfBrackets(key: key) else{ return }
//            appendElemetInValueArray(expression: key)
//            generalCalcStrings.append(key.operation.rawValue)
//            print(generalCalcStrings)
//            
//        case .PERCENT :
//            if (savedValue != "") {
//                if let number = Double(savedValue){
//                    let num :Double  = number * 0.01
//                    savedValue = String(num)
//                }
//            }
//            printResult()
//            
//            
//        case .EQUAL :
//            
//            guard isValidExpression() else {
//                return
//            }
//            let exression = generateString()
//            let fixedExpr = exression.replacingOccurrences(of: "x", with: "*")
//            let result = expressionCalc.calculate(expression: fixedExpr)
//            let doubleResult = Double(result)
//            let remidner = doubleResult?.remainder(dividingBy: 1)
//            var resultStirng = ""
//            if(remidner == 0){
//                resultStirng = String(Int(doubleResult ?? 0.0))
//            }else{
//                resultStirng = String(format:"%.2f",doubleResult!)
//            }
//            
//            viewController?.displayValue(resultStirng)
//            valueArray.removeAll()
//            savedValue = ""
//            
//        case .SIGN:
//            if(savedValue.starts(with: "-") ){
//                savedValue = String(savedValue.dropFirst())
//            }else {
//                savedValue = "-" + savedValue
//            }
//
//        default:
//            guard  isValidDigitwithPoint(key: key.title) else {return}
//            savedValue += key.title
//            printResult()
//            generalCalcStrings.append(key.title)
//        }
//    }
//    
//    
//    
//    private func printResult(){
//        let expression = generateString()
//        viewController?.displayValue(expression)
//    }
//    
//    private func generateString()->String {
//        var strings:[String] = []
//        
//        for value in valueArray{
//            if(value.operation == CalcOperation.DIGIT){
//                strings.append(value.value ?? "")
//            }else{
//                strings.append(value.operation.rawValue)
//            }
//        }
//        
//        strings.append(savedValue)
//        return strings.joined(separator: " ")
//        //return strings
//    }
//    
//    func createStringExpr(){
//        print(valueArray)
//    }
//    
//   
//    
//    
//    static func generateDataSource() -> [CalcButton]{
//        let buttons = [
//            CalcButton(title:"0", backGround: .gray, operation: .DIGIT, fontColor: .white,fontStyle: .systemFont(ofSize: 45,weight: .light) ),
//            CalcButton(title:"AC", backGround: .lightGray, operation: .CLEAN, fontColor: .black, fontStyle:.systemFont(ofSize: 35,weight: .light)),
//            CalcButton(title:"+/-",backGround: .lightGray,operation: .SIGN , fontColor: .black, fontStyle:.systemFont(ofSize: 35,weight: .light)),
//            CalcButton(title:"%", backGround: .lightGray, operation: .PERCENT, fontColor: .black, fontStyle:.systemFont(ofSize: 35,weight: .light)),
//            CalcButton(title:"/", backGround: .orange, operation: .DIVIDE, fontColor: .white, fontStyle:.systemFont(ofSize: 35,weight: .light)),
//            CalcButton(title:"7", backGround: .lightGray, operation: .DIGIT, fontColor: .black, fontStyle:.systemFont(ofSize: 35,weight: .light)),
//            CalcButton(title:"8", backGround: .lightGray, operation: .DIGIT, fontColor: .black, fontStyle:.systemFont(ofSize: 35,weight: .light)),
//            CalcButton(title:"9", backGround: .lightGray, operation: .DIGIT, fontColor: .black, fontStyle:.systemFont(ofSize: 35,weight: .light)),
//            CalcButton(title:"x", backGround: .orange, operation: .MULTIPLY, fontColor: .white, fontStyle:.systemFont(ofSize: 35,weight: .light)),
//            CalcButton(title:"4", backGround: .lightGray, operation: .DIGIT, fontColor: .black, fontStyle:.systemFont(ofSize: 35,weight: .light)),
//            CalcButton(title:"5", backGround: .lightGray, operation: .DIGIT, fontColor: .black, fontStyle:.systemFont(ofSize: 35,weight: .light)),
//            CalcButton(title:"6", backGround: .lightGray, operation: .DIGIT, fontColor: .black, fontStyle:.systemFont(ofSize: 35,weight: .light)),
//            CalcButton(title:"-", backGround: .orange, operation: .MINUS, fontColor: .white, fontStyle:.systemFont(ofSize: 35,weight: .light)),
//            CalcButton(title:"1", backGround: .lightGray, operation: .DIGIT, fontColor: .black, fontStyle:.systemFont(ofSize: 35,weight: .light)),
//            CalcButton(title:"2", backGround: .lightGray, operation: .DIGIT, fontColor: .black, fontStyle:.systemFont(ofSize: 35,weight: .light)),
//            CalcButton(title:"3", backGround: .lightGray, operation: .DIGIT, fontColor: .black, fontStyle:.systemFont(ofSize: 35,weight: .light)),
//            CalcButton(title:"+", backGround: .orange, operation: .PLUS, fontColor: .white, fontStyle:.systemFont(ofSize: 35,weight: .light)),
//            CalcButton(title:"0", backGround: .lightGray, operation: .DIGIT, fontColor: .black, fontStyle:.systemFont(ofSize: 35,weight: .light)),
//            CalcButton(title:".", backGround: .lightGray, operation: .DIGIT, fontColor: .black, fontStyle:.systemFont(ofSize: 35,weight: .light)),
//            CalcButton(title:"(", backGround: .lightGray, operation: .LEFTPRANT, fontColor: .black, fontStyle:.systemFont(ofSize: 35,weight: .light)),
//            CalcButton(title:")", backGround: .lightGray, operation: .RIGHTPRANT, fontColor: .black, fontStyle:.systemFont(ofSize: 35,weight: .light)),
//            CalcButton(title:"=", backGround: .orange, operation: .EQUAL, fontColor: .white, fontStyle:.systemFont(ofSize: 35,weight: .light))]
//        
//        
//        return buttons
//    }
//    
//}
//
//
//
