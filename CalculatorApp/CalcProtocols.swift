
import Foundation
protocol CalculatorUIProtocol : AnyObject {
    func displayValue(_ value : String)
}
protocol CalculatorLogicProtocol{
    func didTapOn (key : CalcButton)
}

protocol CalculatorInputValidation{
    func isValidExpression(exp : String) -> Bool
    func upDateInput(key:String, array: inout [CalcValue])
    func getResultString(array : [CalcValue])-> String
    func generateString(_ isDouble:Bool, array : [CalcValue])->String
    func isValidOperation(key:String, array: inout [CalcValue])->Bool
    func isValidDigitwithPoint(key : String, array : [CalcValue])->Bool
    func isValidDigitAfterOperation(key : String, array : [CalcValue]) -> Bool
    func bracketsAmount(array: [CalcValue])->Bool
    func checkIfAfterPointIsDigit (array: [CalcValue], key : CalcButton)-> Bool
    func checkIfLefPranIsAfterOper(array:[CalcValue], key: CalcButton)->Bool
    func limitLengthOfInputDigit(array: [CalcValue], key: CalcButton)->Bool
    
    
}

extension CalculatorInputValidation{
    func isValidExpression(exp: String) -> Bool{
        return true
    }
    
    func ckechInputPrant(array: [CalcValue], key: CalcButton)->Bool{
       return true
    }
    
    func checkInputOperation(array: inout [CalcValue], key: CalcButton)->Bool{
        return true
    }
    
    func checkBracketsAmountWhenEqual (array: [CalcValue], key: CalcButton)->Bool{
        return true
    }
    
    func checkInputDigits(array: [CalcValue], key: CalcButton)->Bool{
      return true
  }
}

