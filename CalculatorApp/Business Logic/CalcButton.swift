//
//  CalcButton.swift
//  CalculatorApp
//
//  Created by Oleh Makhobei on 07.09.2021.
//

import UIKit
import Foundation

//let PLUS = "+"
//let MINUS = "-"
//let MULTIPLY = "x"
//let DIVIDE = "/"
//let PERCENT = "%"
//let EQUAL = "="
//let CLEAN = "AC"
//let DIGIT = ["1","2","3","4","5","6","7","8","9","0","."]
//let SIGN = "+/-"

struct CalcButton {
    let title : String
    let backGround : UIColor
    let operation : CalcOperation
    let fontColor : UIColor
    let fontStyle : UIFont
}

enum CalcOperation:String{
    case PLUS = "+",
         MINUS = "-",
         MULTIPLY = "x",
         DIVIDE = "/",
         PERCENT = "%",
         EQUAL = "=",
         CLEAN = "AC",
         DIGIT = "",
         SIGN = " ",
         LEFTPRANT = "(",
         RIGHTPRANT = ")"
         
}




