//
//  CalculatorValue.swift
//  CalculatorApp
//
//  Created by Oleh Makhobei on 13.09.2021.
//

import Foundation
import UIKit

struct CalcValue :Equatable{
    var operation : CalcOperation
    var value : String?
}
