//
//  CollectionViewCell.swift
//  CalculatorApp
//
//  Created by Olena Makhobei on 25.08.2021.
//

import UIKit

class CollectionViewCell: UICollectionViewCell{
 
    
    @IBOutlet var calcLbl: UILabel!
    
func configure(with calculatorButton : CalcButton){
    calcLbl.text = calculatorButton.title
    calcLbl.backgroundColor = calculatorButton.backGround
    calcLbl.textColor = calculatorButton.fontColor
    calcLbl.font = calculatorButton.fontStyle
    calcLbl.adjustsFontSizeToFitWidth = true
    calcLbl.translatesAutoresizingMaskIntoConstraints = false
    
    
    }
    
}
